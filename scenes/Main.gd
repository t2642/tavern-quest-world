extends Node

var time = 0
onready var worldTimeLabel = $WorldMap/UI/PanelContainer/NinePatchRect/WorldTimeLabel

func _process(delta):
	time += delta * 1.0
	show_world_timer()
	
func show_world_timer():
	worldTimeLabel.text = String(int(time))
